import React from 'react';
import PropTypes from 'prop-types';
import TagsInput from 'react-tagsinput';
import { Icon, Label } from 'semantic-ui-react';

const Tag = ({
  tag,
  key,
  disabled,
  onRemove,
  classNameRemove,
  getTagDisplayValue,
  ...other
}) => (
  <Label key={key} {...other}>
    {getTagDisplayValue(tag)}
    {!disabled && (
      <Icon
        name="delete"
        className={classNameRemove}
        onClick={() => onRemove(key)}
      />
    )}
  </Label>
);

Tag.propTypes = {
  tag: PropTypes.string.isRequired,
  key: PropTypes.string.isRequired,
  disabled: PropTypes.bool.isRequired,
  onRemove: PropTypes.func.isRequired,
  classNameRemove: PropTypes.string.isRequired,
  getTagDisplayValue: PropTypes.func.isRequired,
};

const TagGroup = ({ tags, setTags }) => (
  <TagsInput
    className=""
    value={tags}
    onChange={setTags}
    renderTag={Tag}
  />
);

TagGroup.propTypes = {
  tags: PropTypes.arrayOf(PropTypes.string),
  setTags: PropTypes.func.isRequired,
};

TagGroup.defaultProps = {
  tags: [],
};

export default TagGroup;
