import {
  always,
  applySpec,
  filter,
  over,
  add,
  contains,
  lensPath,
  when,
  is,
  reject,
  assocPath,
  equals,
  compose,
  __,
  prop,
  merge,
  concat,
  defaultTo,
  map,
  indexBy,
  union,
} from 'ramda';
import {
  FLICKR_SEARCH,
  ADD_TAG,
  REMOVE_TAG,
  SET_RESULTS,
  FLICKR_SEARCH_ERROR,
  CLEAR_CACHE,
  FLICKR_LOAD_MORE,
  SET_TAGS,
  FLICKR_SET_SORT,
  FLICKR_IMAGE_LOADING,
  FLICKR_SET_IMAGE_INFO,
} from './actions';

export const initialSearchPayload = {
  isLoading: true,
  currentPage: 1,
  results: [],
  error: undefined,
};

const reducer = {
  [FLICKR_SEARCH]: (state, { payload }) => over(
    lensPath(['search']),
    merge(__, {
      ...payload,
      ...initialSearchPayload,
    }),
    state,
  ),
  [FLICKR_SEARCH_ERROR]: (state, { message }) => compose(
    assocPath(['search', 'error'], message),
    assocPath(['search', 'isLoading'], false),
  )(state),
  [ADD_TAG]: (state, { tag }) => compose(
    over(
      lensPath(['search']),
      merge(__, {
        ...initialSearchPayload,
      }),
    ),
    over(lensPath(['search', 'tags']), union(__, [tag])),
  )(state),
  [SET_TAGS]: (state, { tags }) => compose(
    over(
      lensPath(['search']),
      merge(__, {
        ...initialSearchPayload,
      }),
    ),
    assocPath(['search', 'tags'], tags),
  )(state),
  [REMOVE_TAG]: (state, { tag }) => compose(
    over(
      lensPath(['search']),
      merge(__, {
        ...initialSearchPayload,
      }),
    ),
    over(lensPath(['search', 'tags']), reject(equals(tag))),
  )(state),
  [SET_RESULTS]: (state, {
    images, pageCount, clear, totalCount,
  }) => compose(
    assocPath(['search', 'totalCount'], totalCount),
    assocPath(['search', 'pageCount'], pageCount),
    assocPath(['search', 'isLoading'], false),
    over(
      lensPath(['images']),
      compose(
        merge(
          compose(
            indexBy(prop('id')),
            map(
              applySpec({
                id: prop('id'),
                title: prop('title'),
                url: ({ server, id, secret }) => `https://live.staticflickr.com/${server}/${id}_${secret}_q.jpg`,
              }),
            ),
          )(images),
        ),
        defaultTo({}),
      ),
    ),
    over(
      lensPath(['search', 'results']),
      compose(
        concat(__, images.map(prop('id'))),
        (clear ? always : defaultTo)([]),
      ),
    ),
  )(state),
  [CLEAR_CACHE]: (state, { count }) => over(
    lensPath(['images']),
    when(
      is(Array),
      filter(({ id }) => contains(id, state.search.results) || count-- <= 0),
    ),
    state,
  ),
  [FLICKR_LOAD_MORE]: compose(
    over(lensPath(['search', 'currentPage']), add(1)),
    assocPath(['search', 'isLoading'], true),
  ),
  [FLICKR_SET_SORT]: (state, { value }) => compose(
    assocPath(['search', 'sortValue'], value),
    over(
      lensPath(['search']),
      merge(__, {
        ...initialSearchPayload,
      }),
    ),
  )(state),
  [FLICKR_IMAGE_LOADING]: (state, { imageId }) => assocPath(['images', imageId, 'isLoading'], true, state),
  [FLICKR_SET_IMAGE_INFO]: (state, { imageId, payload }) => over(
    lensPath(['images', imageId]),
    merge(__, {
      ...payload,
      hasInfo: true,
      isLoading: false,
    }),
    state,
  ),
};

export default (state = initialSearchPayload, action) => {
  if (reducer[action.type]) {
    return reducer[action.type](state, action);
  }
  return state;
};
