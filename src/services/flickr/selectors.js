import * as R from 'ramda';
import moment from 'moment';

export const getTags = R.path(['search', 'tags']);

export const getSearchQuery = R.path(['search', 'query']);

export const getSearchResults = R.pathOr([], ['search', 'results']);

export const getIsLoading = R.pathOr(false, ['search', 'isLoading']);

export const getErrorMessage = R.path(['search', 'error']);

export const getCurrentPage = R.path(['search', 'currentPage']);

export const getPageCount = R.path(['search', 'pageCount']);

export const getTotalCount = R.path(['search', 'totalCount']);

export const getSortValue = R.path(['search', 'sortValue']);

export const extractImageInfo = R.applySpec({
  tags: R.compose(
    R.pluck('raw'),
    R.pathOr([], ['photo', 'tags', 'tag']),
  ),
  profileName: R.pathOr('N/A', ['photo', 'owner', 'username']),
  profileLink: R.compose(
    (profileId) => (profileId ? `https://www.flickr.com/people/${profileId}/` : '#'),
    R.path(['photo', 'owner', 'nsid']),
  ),
  link: R.pathOr('#', ['photo', 'urls', 'url', 0, '_content']),
  location: R.compose(
    R.when(
      R.isEmpty,
      R.always(undefined),
    ),
    R.path(['photo', 'owner', 'location']),
  ),
  date: R.compose(
    (unixTimestamp) => moment.unix(unixTimestamp).format('Do MMM YYYY'),
    R.path(['photo', 'dates', 'posted']),
  ),
});
